from django.conf.urls import url
from src.api.v1.views.saveFile import SaveFileView
from src.api.v1.views.decodeView import DecodeView
from src.api.v1.views.audioiphone import AudioInfoIphoneView
from src.api.v1.views.triggerinfoview import TriggerInfoView
from src.api.v1.views.correlationView import CorrelationView

urlpatterns = [
    url(r'^v1/audio/info/$', SaveFileView.as_view()),
    url(r'^v1/correlation/$', CorrelationView.as_view()),
    url(r'^v1/generateOTP/$', CorrelationView.as_view()),
    url(r'^v1/trigger/info/$', TriggerInfoView.as_view()),
    url(r'^v1/iphone/audio/info/$', AudioInfoIphoneView.as_view()),
    url(r'^v1/trigger/value/$', SaveFileView.as_view()),
    url(r'^v1/trigger/test/$', SaveFileView.as_view()),
    url(r'^v1/decode/audio/$', DecodeView.as_view()),
    ]

