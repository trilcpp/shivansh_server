from rest_framework import generics
from rest_framework  import mixins
from rest_framework.status import HTTP_201_CREATED
from src.api.v1.libraries.customresponse import CustomResponse
from src.api.v1.libraries.loggingmixin import LoggingMixin
from django_mysqlpool import auto_close_db
from django.core.exceptions import ValidationError
import os
import numpy as np
from scipy.io.wavfile import write
import json
'''
{
"data": [chunks]
}
'''

class AudioInfoIphoneView( LoggingMixin, generics.GenericAPIView, mixins.UpdateModelMixin, mixins.DestroyModelMixin, mixins.ListModelMixin):

    @auto_close_db
    def post(self, request):
        try:
            details = request.data.copy()
            path, dirs, files = os.walk("iphonelogs").next()
            test_name = "default_" + str(len(files) + 1)
            # file_to_write = "iphonelogs/" + test_name + ".json" #dumb mistakes happen !
            file_wav = "iphonelogs/" + test_name + ".wav"
            if "vec" not in details:
                raise ValidationError("vec Not Found")
            if "decoded" not in details:
                raise ValidationError("decoded Not Found")


            if details["vec"] != "" :
                #COnverts to .wav
                data = details["vec"]
                data = data[:-1]
                array_vec = np.fromstring(data, dtype=float, sep=' ')
                array_vector = np.array(array_vec, np.float)
                write(file_wav, 44100, array_vector)

                # with open(file_to_write, 'w') as outfile:
                #     json.dump(details["vec"], outfile, sort_keys=True, indent=4,
                #               ensure_ascii=False)

            del details["vec"]
            with open("/home/shivujagga/Softwares/Server/testingframework/TestingIphone.log", 'a') as outfile:
                json.dump(details, outfile, sort_keys=True, ensure_ascii=False)
                outfile.write("\n")


            return CustomResponse(message='Success', payload={"result":"exce"}, code=HTTP_201_CREATED)
        except Exception as e:
            print "except", e
            raise ValidationError("ERROR")
