from django_mysqlpool import auto_close_db
from rest_framework import generics
from rest_framework import mixins
from rest_framework.status import HTTP_201_CREATED, HTTP_200_OK
from src.api.v1.libraries.customresponse import CustomResponse
from src.api.v1.libraries.loggingmixin import LoggingMixin
from src.common.libraries.constants import *
from scipy.io.wavfile import write
import numpy as np
import os, json
'''
{
    "email": "scoder91@gmail.com",
    "password_hash": "12345678"
}
'''


class TriggerTestView(LoggingMixin, generics.GenericAPIView, mixins.CreateModelMixin, mixins.DestroyModelMixin):

    @auto_close_db
    def post(self, request):
        data = request.data.copy()
        fileName = data["filename"]
        Response = {}

        return CustomResponse(message="File Save Successfully", payload=Response, code=HTTP_200_OK)