from django_mysqlpool import auto_close_db
from rest_framework import generics
from rest_framework import mixins
from rest_framework.status import HTTP_201_CREATED, HTTP_200_OK
from src.api.v1.libraries.customresponse import CustomResponse
from src.api.v1.libraries.loggingmixin import LoggingMixin
from src.common.libraries.decode import DecodeAudio
'''

'''


class DecodeView(LoggingMixin, generics.GenericAPIView, mixins.CreateModelMixin, mixins.DestroyModelMixin):

    @auto_close_db
    def get(self, request):
        data = request.GET.copy()
        Response = DecodeAudio(file=data["file"]).decode
        return CustomResponse(message="File Save Successfully", payload=Response, code=HTTP_200_OK)