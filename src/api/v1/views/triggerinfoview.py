from django_mysqlpool import auto_close_db
from rest_framework import generics
from rest_framework import mixins
from rest_framework.status import HTTP_201_CREATED, HTTP_200_OK
from src.api.v1.libraries.customresponse import CustomResponse
from src.api.v1.libraries.loggingmixin import LoggingMixin
from src.common.libraries.constants import *
from scipy.io.wavfile import write
import numpy as np
import os, json
from scipy.io.wavfile import read
from src.common.libraries.triggerTest import TriggerTestLib
'''
{
    "email": "scoder91@gmail.com",
    "password_hash": "12345678"
}
'''


class TriggerInfoView(LoggingMixin, generics.GenericAPIView, mixins.CreateModelMixin, mixins.DestroyModelMixin):

    @auto_close_db
    def post(self, request):
        data = request.data.copy()
        Response = []
        file = data["file"]
        try:
            if data["type"] == "iphone":
                Response = json.loads(open(file).read())
                Response["trigger"] = Response["trigger"].split(",")
                data = np.fromstring(Response["vec"], dtype=float, sep=' ')
                print data
                Response["triggerViaPython"] = TriggerTestLib(
                    data=data
                ).getTriggerValues
                del Response["vec"]
            elif data["type"] == "android":
                Response = json.loads(open(file).read())
                data = read(file.replace(".json",".wav"))[1]
                Response["triggerViaPython"] = TriggerTestLib(
                    data=data
                ).getTriggerValues
        except Exception as e:
            print e

        return CustomResponse(message="File Save Successfully", payload=Response, code=HTTP_200_OK)