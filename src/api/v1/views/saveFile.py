from django_mysqlpool import auto_close_db
from rest_framework import generics
from rest_framework import mixins
from rest_framework.status import HTTP_200_OK
from src.api.v1.libraries.customresponse import CustomResponse
from src.api.v1.libraries.loggingmixin import LoggingMixin
from matplotlib import pyplot as plt
from scipy.io.wavfile import write, read
import numpy as np
import os, json
from src.common.libraries.androidrec import SaveDataAndroid
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

'''
{
    "email": "scoder91@gmail.com",
    "password_hash": "12345678"
}
'''


class SaveFileView(LoggingMixin, generics.GenericAPIView, mixins.CreateModelMixin, mixins.DestroyModelMixin):

    # @auto_close_db
    # def get(self, requests):
    #
    #     Test_list = AndroidTest.objects.filter()
    #     paginator = Paginator(Test_list, 200)
    #     page = requests.GET.get('page', 'false')
    #     try:
    #         tests = paginator.page(page)
    #     except PageNotAnInteger:
    #         # If page is not an integer, deliver first page.
    #         tests = paginator.page(1)
    #     except EmptyPage:
    #         # If page is out of range (e.g. 9999), deliver last page of results.
    #         tests = paginator.page(paginator.num_pages)
    #
    #     response = dict()
    #     start, end  = 427, 476
    #     response["count"] = end-start
    #     response["data"] = []
    #     for i in range(start,end+1):
    #         try:
    #             import json
    #             element = dict()
    #             element["json_file"] = "/home/sid/TestingFramework/AndroidJsonFile/default_{}.json".format(str(i))
    #             data = json.loads(open(element["json_file"]).read())
    #
    #             for sub_element in data:
    #
    #                 if "result_chunk" in sub_element:
    #                     selected = {
    #                         "top_20 Algo V1": str(data[sub_element][3]),
    #                         "Shorting Algo V2": str(data[sub_element][0]) + " | " + str(data[sub_element][1]) + " | " + str(
    #                             data[sub_element][2]),
    #                         "Combine Shorting Algo V3": str(data[sub_element][4]) + " | " + str(
    #                             data[sub_element][5]) + " | " + str(data[sub_element][6])
    #                     }
    #                     element[sub_element] = selected
    #
    #             element["test_name"] = "default_{}".format(str(i))
    #             del element['json_file']
    #             response["data"].append(element)
    #
    #         except Exception as e:
    #             print e
    #
    #     # for element in response_data_moto:
    #     #     response["data"].append(element)
    #     return CustomResponse(message="File Save Successfully", payload=response, code=HTTP_200_OK)



    @auto_close_db
    def post(self, request):
        data = request.data.copy()

        audio_chunks = False
        other_data = False

        if "audio_chunks" in data:
            if len(data["audio_chunks"]) > 500:
                audio_chunks = data["audio_chunks"]
                del data["audio_chunks"]

        if len(data) > 0:
            other_data = data

        response = SaveDataAndroid(
            audio_chunks= audio_chunks, data=other_data
        ).save_record

        return CustomResponse(message="File Save Successfully", payload=response, code=HTTP_200_OK)


    # def delete(self, requests):
    #     try:
    #         if requests.data.get("pre", False):
    #             nodes = AndroidTest.objects.all().order_by("-created")
    #             nodes[0].delete()
    #         else:
    #             AndroidTest.objects.all().delete()
    #     except Exception as e:
    #         print e
    #     return CustomResponse(message="File Save Successfully", payload="True", code=HTTP_200_OK)