from django_mysqlpool import auto_close_db
from rest_framework import generics
from rest_framework import mixins
from rest_framework.status import HTTP_201_CREATED, HTTP_200_OK
from src.api.v1.libraries.customresponse import CustomResponse
from src.api.v1.libraries.loggingmixin import LoggingMixin
from src.common.libraries.constants import *
from scipy.io.wavfile import write
import numpy as np
import os, json
'''
{
    "email": "scoder91@gmail.com",
    "password_hash": "12345678"
}
'''


class SaveFileView(LoggingMixin, generics.GenericAPIView, mixins.CreateModelMixin, mixins.DestroyModelMixin):

    @auto_close_db
    def post(self, request):
        data = request.data.copy()
        open('/home/sid/work/localserver/TestServer/TriggerValue/b.log', 'a').write("{0} ".format(str(data["triggerValue"])))
        return CustomResponse(message="File Save Successfully", payload=True, code=HTTP_200_OK)