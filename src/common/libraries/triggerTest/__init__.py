from scipy.io.wavfile import read
import numpy as np

BASE_DIR = "/home/sid/Desktop/trillbit/server_code/audio_logs/"

def plot_fft(received_wave, Fs=44100):
    """

    :param received_wave: wave file data.
    :param Fs: Sampling Rate, default = 44100
    :return: [Frequency, Amplitude]
    """
    Af = np.fft.fft(received_wave)
    Amp = np.abs(Af)
    size1 = len(received_wave)
    freq = np.linspace(0, Fs, size1)
    freq_hf = freq[0:int(size1 / 2)]
    Amp_hf = Amp[0:int(size1 / 2)]

    return freq_hf, Amp_hf

def get_freq_index(freq, size, fs):
    """
    return index of required frequency ie ( size_of_data / fs ) * freq
    :param freq: required frequency index
    :param size: size of data
    :param fs: sample rate
    :return: index
    """
    return int( round( (size/float(fs)) * freq) )

def get_filterd_fft(signal):
    """
    function filter freq from 0-14k as 0 and 14k - 22.5k given
    :param signal: recieved signal
    :return:
    """
    freq_hf, Amp_hf = plot_fft(signal)
    amp_filtered = []
    for freq, amp in zip(freq_hf, Amp_hf):
        if freq > 13999 and freq < 19000:
            amp_filtered.append(amp)
        else:
            amp_filtered.append(0)

    # # get index of 14k freq
    # freq_index_14k = get_freq_index(freq=14000, size=len(signal), fs=44100)
    #
    # # create a array filtered amp with 0-14k freq as 0
    # filtered_amp = np.repeat([0], freq_index_14k)
    #
    # # append rest 14k - 22.5k
    # filtered_amp = np.append(filtered_amp, Amp_hf[freq_index_14k:])
    # amp_filtered = np.array(filtered_amp)

    return freq_hf, Amp_hf, amp_filtered

def create_chunks_of_filter_fft(freq_hf, amp_hf, chunk_size=5):
    """
    making size of X to X / chunksize
    :param freq_hf: frequency array
    :param amp_hf: amplitude array
    :param chunk_size: chunk_size desired
    :return: chunk_values_amp, chunk_values_freq, freq_14k_index, freq_16k_index, freq_18k_index
    """
    chunk_values_amp = []
    chunk_values_freq = []
    freq_14k_index = -1
    freq_16k_index = -1
    freq_18k_index = -1
    counter = 0
    for i in range(0, len(freq_hf), chunk_size):
        chunk_freq = np.average(freq_hf[i: i+chunk_size])
        if chunk_freq > 13999 and freq_14k_index < 0:
            freq_14k_index = counter

        if chunk_freq > 15999  and freq_16k_index < 0:
            freq_16k_index = counter

        if chunk_freq > 17999 and freq_18k_index < 0:
            freq_18k_index = counter


        chunk_amp  = np.average(amp_hf[i: i+chunk_size])
        chunk_values_amp.append(chunk_amp)
        chunk_values_freq.append(chunk_freq)
        counter += 1

    return chunk_values_amp, chunk_values_freq, freq_14k_index, freq_16k_index, freq_18k_index

def get_all_value(received_signal, G, H, chunk_shrink=1):

    # get filtered amplitude having 0-14k freq as 0.
    freq_hf, Amp_hf, amp_filtered = get_filterd_fft(
        signal=received_signal)

    # shringking chunks from size to size / chunk_shrink
    chunk_amp, chunk_freq, freq_14k_index, freq_16k_index, freq_18k_index = create_chunks_of_filter_fft(
        freq_hf=freq_hf, amp_hf=amp_filtered, chunk_size=1)


    # mean of all frequency from 14k - 22.5k
    mean_value = np.average(chunk_amp[freq_14k_index:])

    # calculating suqare ( x[i] - mean )
    diversion_values_square = []
    for freq, amp in zip(chunk_freq[freq_14k_index:], chunk_amp[freq_14k_index:]):
        diversion_values_square.append(np.square(amp-mean_value))

    # sigma = square_root ( avg ( square ( x[i] - mean ) ) )
    sigma    = np.sqrt(np.average(diversion_values_square))

    # sigma_sq = avg ( square ( x[i] - mean ) ) sigma_square.
    sigma_sq = np.average(diversion_values_square)

    # gamma = ( G * sigma_square ) + ( H * sigma ) / mean
    gama = np.divide(((G * sigma_sq) + (H * sigma)), mean_value)

    from matplotlib import pyplot as plt
    # plt.figure()
    # plt.title("Unfiltered FFT Data")
    # plt.plot(freq_hf, Amp_hf)


    # plt.figure()
    # plt.title("Original Signal")
    # # plt.ylim(0,800)
    # plt.plot(freq_hf, amp_filtered)
    # plt.plot(freq_hf, np.repeat(gama, len(freq_hf)), color='g')
    # plt.plot(freq_hf, np.repeat(mean_value, len(freq_hf)), color='r')


    # initilizing gama values for 14k-16k and 16k-18k.
    gamma_values_14k = [0.1]
    gamma_values_16k = [0.1]

    # checking conditions for different freq range (14k-16k) , (16k,18k) :: absolute(mean_value - amp) > gamma value
    for freq, amp in zip(chunk_freq[freq_14k_index:freq_18k_index], chunk_amp[freq_14k_index:freq_18k_index]):
        if freq < 16000:
            if (abs(mean_value - amp) > gama):
                gamma_values_14k.append(amp)
        else:
            if (abs(mean_value - amp) > gama):
                gamma_values_16k.append(amp)

    # taking avg value from array for each frequency range
    # print mean_value
    # print gama
    gamma_value_14k = np.average(gamma_values_14k)
    gamma_value_16k = np.average(gamma_values_16k)

    return gamma_value_14k, gamma_value_16k

class TriggerTestLib():

    def __init__(self, **kwargs):
        self.data = kwargs["data"]

    @property
    def getTriggerValues(self):
        G = .55
        H = 1.1
        # for file in files:
        received_signal = self.data
        ratio = []
        # gamma_value_14k, gamma_value_16k = get_all_value(received_signal=received_signal[:19000], G=G, H=H)
        for i in range(0, len(received_signal) / 4096):
            start = i * 4096
            end = start + 4096
            # print start, end
            dataNow = received_signal[start:end]
            gamma_value_14k, gamma_value_16k = get_all_value(received_signal=dataNow, G=G, H=H)
            # print gamma_value_14k, gamma_value_16k
            # print (gamma_value_16k / float(gamma_value_14k))
            ratio.append(gamma_value_16k / gamma_value_14k)
        #
        # # # gamma_value_14k, gamma_value_16k = get_all_value(received_signal=received_signal[0:4096], G=G, H=H)
        # from matplotlib import pyplot as plt
        # plt.stem(ratio)
        # plt.show()

        return ratio

if __name__ == '__main__':
    files = "/home/sid/TestingFramework/rec/default_422.wav"
    # files = "/home/sid/work/newAlgo/PN61.wav"
    G = .55
    H = 1.1
    # for file in files:
    filename = files
    received_signal = read(filename)[1]
    ratio = []
    # gamma_value_14k, gamma_value_16k = get_all_value(received_signal=received_signal[:19000], G=G, H=H)
    for i in range(0, len(received_signal)/4096):
        start = i * 4096
        end = start + 4096
        # print start, end
        dataNow = received_signal[start:end]
        gamma_value_14k, gamma_value_16k = get_all_value(received_signal=dataNow, G=G, H=H)
        # print gamma_value_14k, gamma_value_16k
        print (gamma_value_16k / float(gamma_value_14k))
        ratio.append(gamma_value_16k / gamma_value_14k)

    # # gamma_value_14k, gamma_value_16k = get_all_value(received_signal=received_signal[0:4096], G=G, H=H)
    from matplotlib import pyplot as plt
    plt.stem(ratio)
    plt.show()
    # plt.ylim(0,15)
    # plt.show()
