import numpy as np
from scipy.io.wavfile import read
from matplotlib import pyplot as plt
import json

PEAKS_SELECT = 20

class DecodeAudio():

    def __init__(self, **kwargs):
        self.kwargs = kwargs

    def detectPeak(self, corr_op):
        seq = np.argsort(corr_op)[:-PEAKS_SELECT]
        for i in range(0, PEAKS_SELECT):
            for j in range(0, PEAKS_SELECT):
                peak1Selected = seq[PEAKS_SELECT - 1 - i]
                peak2Selected = seq[PEAKS_SELECT - 1 - j]

                disAbs = abs(peak1Selected - peak2Selected) - 1206
                if disAbs > 10 and disAbs < 9000:
                    if (disAbs % 23 == 0) and disAbs > 0 and disAbs < 9000:
                        if (peak1Selected > peak2Selected):
                            return disAbs + 1206, peak2Selected, peak1Selected
                        return disAbs + 1206, peak1Selected, peak2Selected

        return -1, -1, -1

    def getPeaks(self, data, syncData, sepration, origin_from_zero=True):
        Chunk1 = data[0:sepration]
        Chunk2 = data[sepration:]

        corr_op_1 = np.correlate(Chunk1, syncData)
        corr_op_2 = np.correlate(Chunk2, syncData)

        result_1, p1, p2 = self.detectPeak(corr_op_1)
        result_2, p3, p4 = self.detectPeak(corr_op_2)

        if origin_from_zero:
            p3 = p3 + sepration
            p4 = p4 + sepration

        return result_1, result_2, p1, p2, p3, p4

    @property
    def decode(self):
        sync = "/home/sid/work/23otp_4times/distestSync.wav"
        syncData = read(sync)[1]
        main = self.kwargs['file']
        mainData = read(main)[1]

        corr_op = np.correlate(mainData, syncData)
        x_axis = np.linspace(0, len(mainData) - 1, len(mainData))
        result_1, result_2, p1, p2, p3, p4 = self.getPeaks(data=mainData, syncData=syncData, sepration=27000)
        Response = {
            "corr_op": [
                x_axis,
                corr_op
            ],
            "result_1": result_1,
            "result_2": result_2,
            "peak_1": p1,
            "peak_2": p2,
            "peak_3": p3,
            "peak_4": p4
        }
        return Response




