import os
import numpy as np
from scipy.io.wavfile import write

class SaveFile():

    def __init__(self, **kwargs):
        config = {
            "recDir": "rec/default_"
        }
        self.kwargs = kwargs
        self.config = config
        self.audioChunks = kwargs.get("audio_chunks", False)
        self.playedFile = kwargs["played_file"]
        self.recordingDir = config["recDir"]



    def save(self):
        REC_DIR = "/home/sid/TestingFramework/rec/"
        path, dirs, files = os.walk(REC_DIR).next()

        if self.audioChunks:
            testName = self.recordingDir + str(len(files) + 1) + ".wav"
            chunks = np.array(self.audioChunks)
            write(testName, 44100, chunks)




