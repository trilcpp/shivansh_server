import os, json
import numpy as np
from scipy.io.wavfile import write
# from src.common.models import AndroidTest

class SaveDataAndroid():

    def __init__(self, **kwargs):

        self.audio_chunks = kwargs.get("audio_chunks", False)
        self.other_data   = kwargs.get("data", False)
        self.response     = dict()

        self.audioDir = "AndroidpeakTest/audio"
        self.jsonDir = "AndroidpeakTest/json"

        self.addDB = False

    @property
    def save_record(self):
        if self.audio_chunks:
            self.save_audio()

        if self.other_data:
            self.save_json()

        if self.addDB:
            self.save_in_db()

        return self.response

    def save_audio(self):
        '''
        Saving Audio files is audio chunks in wav file.
        :return:
        '''
        path, dirs, files = os.walk(self.audioDir).next()
        test_name = self.audioDir + "/default_" + str(len(files) + 1) + ".wav"
        chunks = np.array(self.audio_chunks)
        write(test_name, 44100, chunks)

        self.response["audio_file"] = test_name

        return True

    def save_json(self):
        path, dirs, files = os.walk(self.jsonDir).next()
        test_name = self.jsonDir + "/default_" + str(len(files) + 1) + ".json"
        with open(test_name, 'w') as outfile:
            json.dump(self.other_data, outfile, sort_keys=True, indent=4,
                      ensure_ascii=False)


        self.response["json_file"] = test_name

        print json.dumps(self.other_data, indent=1)
        return True

    def save_in_db(self):
        try:
            pass
            # AndroidTest.objects.create(**self.response)
        except Exception as e:
            print e

        return True