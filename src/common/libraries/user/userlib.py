import hashlib, requests
from django.core import exceptions as django_exc
from rest_framework.status import HTTP_400_BAD_REQUEST
from src.common.helpers import validators
from src.common.libraries.constants import *
from src.common.models import User , Token
from src.api.v1.serializers.usersserializer import UsersSerializer

class UserLib():

    def get_user(self, user, options):
        fields = (KEY_USER_ID, KEY_USER_NAME, KEY_EMAIL_ID)
        return UsersSerializer(user, fields=fields).data

    def authenticate_social_login(self, id, token):
        login_info = self.get_login_info(token=token)
        if login_info['id'] == id:
            if User.objects.filter(facebook_id=id).exists():
                try:
                    User.objects.filter(facebook_id=id).update(facebook_token=token)

                    user = User.objects.get(facebook_id=id)
                    token, created = Token.objects.get_or_create(user=user)
                    return (token. access_token, created)
                except Exception as e:
                    print e
        raise exceptions.ValidationError('Invalid login info', code=HTTP_400_BAD_REQUEST)

    def get_login_info(self, token):
        try:

            URL = '{0}{1}{2}'.format(FB_LOGIN_URL, FB_API_LOGIN_FIELDS, token)
            data = requests.get(URL)
            return data.json()
        except Exception as e:
            raise exceptions.ValidationError('Invalid Facebook Token', code=HTTP_400_BAD_REQUEST)





