from django.db import models
from src.common.libraries.constants import *
import binascii, os, uuid


class UserManager(models.Manager):
    def generate_userid(self):
        return str(uuid.uuid4())

class User(models.Model):
    name            = models.CharField(max_length=MAX_NAME_LENGTH)
    email           = models.EmailField(max_length=MAX_EMAIL_LENGTH, unique=True)
    facebook_id     = models.CharField(max_length=SOCIAL_ID_LENGTH, default='Not Avalible')
    facebook_token  = models.CharField(max_length=1000, default='Not Avalible')
    user_id         = models.CharField(max_length=UID_LENGTH, primary_key=True, editable=False)
    created         = models.DateTimeField(auto_now_add=True)
    objects         = UserManager()

    def is_authenticated(self):
        """
        Always return True. This is a way to tell if the user has been
        authenticated in templates.
        """
        return True

    def save(self, *args, **kwargs):
        if not self.user_id:
            self.user_id = User.objects.generate_userid()
        return super(User, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.user_id

    class Meta:
        db_table = 'user'
        app_label = 'common'