import numpy as np
NO_OF_PEAKS = 20
def getPeaks(corr_op, constantVal=1206):
    seq = np.argsort(corr_op)[-NO_OF_PEAKS:]
    for i in xrange(NO_OF_PEAKS):
        for j in xrange(NO_OF_PEAKS):
            if i != j:
                abs_Distance = abs(seq[i] - seq[j])
                if (abs_Distance > 3000 and (abs_Distance-3000) % 23 == 0):
                    peak_1 =
