import numpy as np
from scipy.io.wavfile import read

syncFile = ""
syncData = read(syncFile)[1]
NO_OF_TOP_PEAKS = 20

def detectDataStart(data, minimumDis):
    corr_op = np.correlate(data, syncFile)
    seq = np.argsort(corr_op)[:-20]
    for i in range(0, NO_OF_TOP_PEAKS):
        for j in range(0, NO_OF_TOP_PEAKS):
            distance = abs(seq[i] - seq[j])
            if distance == minimumDis:
                if seq[i]> seq[j]:
                    return seq[i]
                return seq[j]

    return -1